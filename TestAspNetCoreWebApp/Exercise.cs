namespace TestAspNetCoreWebApp
{
    public class Exercise
    {
        public string Name { get; set; }

        public int Count { get; set; }
        
        public Exercise(string name, int count)
        {
            Name = name;
            Count = count;
        }
    }
}