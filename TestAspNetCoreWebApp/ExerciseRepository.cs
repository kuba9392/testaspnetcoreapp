using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace TestAspNetCoreWebApp
{
    public class ExerciseRepository
    {
        private List<Exercise> exerciseList;

        public ExerciseRepository()
        {
            this.exerciseList.Add(new Exercise("test", 1234));
        }

        public Exercise FindOne(string name)
        {
            foreach (var exercise in this.exerciseList)
            {
                if (exercise.Name == name)
                {
                    JsonConvert.PopulateObject("a", exercise);
                    Console.WriteLine(exercise);
                    return exercise;
                }
            }

            return null;
        }

        public void Save(Exercise exercise)
        {
            this.exerciseList.Add(exercise);
        }
    }
}