FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY TestAspNetCoreWebApp/*.csproj ./TestAspNetCoreWebApp/
RUN dotnet restore

# copy everything else and build app
COPY TestAspNetCoreWebApp/. ./TestAspNetCoreWebApp/
WORKDIR /app/TestAspNetCoreWebApp
RUN dotnet publish -c Release -o out


FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/TestAspNetCoreWebApp/out ./
ENTRYPOINT ["dotnet", "TestAspNetCoreWebApp.dll"]