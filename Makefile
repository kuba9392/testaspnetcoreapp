export PROJECT=exercise_api
export VERSION=0.1

export IMAGE=localhost:5000/${PROJECT}:${VERSION}


build:
	docker build --file Dockerfile -t ${IMAGE} .
	docker push ${IMAGE}
	
deploy:
	docker service create -p 26000:80 --name ${PROJECT} ${IMAGE}
	
clean:
	docker service rm ${PROJECT}